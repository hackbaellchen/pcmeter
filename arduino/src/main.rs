#![no_std]
#![no_main]

use arduino_hal::hal::wdt;
use arduino_hal::{
    hal::port::{PB1, PB2, PB3, PD3, PD5, PD6},
    port::{mode::PwmOutput, Pin},
    simple_pwm::{IntoPwmPin, Timer0Pwm, Timer1Pwm, Timer2Pwm},
};
use core::cmp::min;
use embedded_hal::serial::Read;
use nb::block;
use panic_halt as _;
use ufmt::derive::uDebug;

#[derive(uDebug)]
struct MeterAndLEDSettings {
    cpu_min: u8,
    cpu_max: u8,
    ram_min: u8,
    ram_max: u8,
    cpu_green: u8,
    cpu_red: u8,
    ram_green: u8,
    ram_red: u8,
    threshold: u8,
}

struct MetersAndLEDs {
    cpu_meter: Pin<PwmOutput<Timer0Pwm>, PD5>,
    cpu_led_green: Pin<PwmOutput<Timer2Pwm>, PD3>,
    cpu_led_red: Pin<PwmOutput<Timer0Pwm>, PD6>,
    ram_meter: Pin<PwmOutput<Timer1Pwm>, PB2>,
    ram_led_green: Pin<PwmOutput<Timer1Pwm>, PB1>,
    ram_led_red: Pin<PwmOutput<Timer2Pwm>, PB3>,
}

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let timer0 = Timer0Pwm::new(dp.TC0, arduino_hal::simple_pwm::Prescaler::Prescale64);
    let timer1 = Timer1Pwm::new(dp.TC1, arduino_hal::simple_pwm::Prescaler::Prescale64);
    let timer2 = Timer2Pwm::new(dp.TC2, arduino_hal::simple_pwm::Prescaler::Prescale64);

    let mut pwm = MetersAndLEDs {
        cpu_meter: pins.d5.into_output().into_pwm(&timer0),
        cpu_led_green: pins.d3.into_output().into_pwm(&timer2),
        cpu_led_red: pins.d6.into_output().into_pwm(&timer0),
        ram_meter: pins.d10.into_output().into_pwm(&timer1),
        ram_led_green: pins.d9.into_output().into_pwm(&timer1),
        ram_led_red: pins.d11.into_output().into_pwm(&timer2),
    };
    pwm.cpu_meter.enable();
    pwm.ram_meter.enable();

    let mut serial = arduino_hal::default_serial!(dp, pins, 115200);

    // default values
    let mut settings = MeterAndLEDSettings {
        cpu_min: 0,
        cpu_max: 255,
        ram_min: 0,
        ram_max: 255,
        cpu_green: 15,
        cpu_red: 200,
        ram_green: 15,
        ram_red: 200,
        threshold: 80,
    };

    // Reset nach 4 Sekunden ohne feed
    let mut watchdog = wdt::Wdt::new(dp.WDT, &dp.CPU.mcusr);
    watchdog.start(wdt::Timeout::Ms4000).unwrap();

    loop {
        // uwriteln!(&mut serial, "Ready to read.").void_unwrap();
        match block!(serial.read()).unwrap() {
            b'C' => {
                // C = Konfiguration
                // uwriteln!(&mut serial, "Got configuration.").void_unwrap();
                settings = MeterAndLEDSettings {
                    cpu_min: block!(serial.read()).unwrap(),
                    cpu_max: block!(serial.read()).unwrap(),
                    ram_min: block!(serial.read()).unwrap(),
                    ram_max: block!(serial.read()).unwrap(),
                    cpu_green: block!(serial.read()).unwrap(),
                    cpu_red: block!(serial.read()).unwrap(),
                    ram_green: block!(serial.read()).unwrap(),
                    ram_red: block!(serial.read()).unwrap(),
                    threshold: block!(serial.read()).unwrap(),
                };
                watchdog.feed();
                // uwriteln!(&mut serial, "Received configuration: {:#?}.", settings).void_unwrap();
            }
            b'V' => {
                // V = Values
                let cpu = min(block!(serial.read()).unwrap(), 100);
                let ram = min(block!(serial.read()).unwrap(), 100);

                let cpu_meter_value =
                    cpu as f32 * (settings.cpu_max as f32 - settings.cpu_min as f32) / 100 as f32
                        + settings.cpu_min as f32;
                let ram_meter_value =
                    ram as f32 * (settings.ram_max as f32 - settings.ram_min as f32) / 100 as f32
                        + settings.ram_min as f32;

                pwm.cpu_meter.set_duty(min(cpu_meter_value as u8, 255));
                pwm.ram_meter.set_duty(min(ram_meter_value as u8, 255));

                if cpu < settings.threshold {
                    pwm.cpu_led_red.disable();
                    pwm.cpu_led_green.enable();
                    pwm.cpu_led_green.set_duty(settings.cpu_green);
                } else {
                    pwm.cpu_led_green.disable();
                    pwm.cpu_led_red.enable();
                    pwm.cpu_led_red.set_duty(settings.cpu_red);
                }

                if ram < settings.threshold {
                    pwm.ram_led_red.disable();
                    pwm.ram_led_green.enable();
                    pwm.ram_led_green.set_duty(settings.ram_green);
                } else {
                    pwm.ram_led_green.disable();
                    pwm.ram_led_red.enable();
                    pwm.ram_led_red.set_duty(settings.ram_red);
                }
                watchdog.feed();
                // uwriteln!(&mut serial, "Showing values: {}, {}.", cpu, ram).void_unwrap();
            }
            b'\n' | b'\r' => {
                // uwriteln!(&mut serial, "CR/LF ignored.").void_unwrap();
            }
            _ => {
                // uwriteln!(&mut serial, "Read garbage!").void_unwrap();
            }
        }
    }
}
