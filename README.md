# PCMeter

Rust-powered analog 5V meters to display CPU and RAM usage.

![front picture](img/front.jpg)

The client code has been tested for Windows 10, but should compile for other operating systems as well.

An Arduino Nano is used as the microcontroller. Adjust your cargo settings accordingly.

## Credits

Original idea and icon by [Scott W. Vincent](http://www.swvincent.com/pcmeter/index.html)
