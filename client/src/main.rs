#![windows_subsystem = "windows"]

use std::{fs, thread, time::Duration};

use anyhow::anyhow;
use serde::Deserialize;
use serialport::SerialPort;
use sysinfo::{CpuRefreshKind, MemoryRefreshKind, RefreshKind, System};

#[derive(Deserialize, Debug)]
struct SerialProperties {
    port: String,
    baud: u32,
    threshold_percent: u8,
    samples_per_send: u8,
    sends_per_second: u8,
    meter_calibration_8bit: MeterCalibration,
    led_brightness_8bit: LEDBrightness,
}

#[derive(Deserialize, Debug)]
struct MeterCalibration {
    cpu_min: u8,
    cpu_max: u8,
    mem_min: u8,
    mem_max: u8,
}

#[derive(Deserialize, Debug)]
struct LEDBrightness {
    cpu_green: u8,
    cpu_red: u8,
    mem_green: u8,
    mem_red: u8,
}

struct AverageValues {
    cpu: u8,
    mem: u8,
}

#[tracing::instrument]
fn load_config() -> anyhow::Result<SerialProperties> {
    let json = fs::read_to_string("config.json")?;
    Ok(serde_json::from_str::<SerialProperties>(&json)?)
}

#[tracing::instrument]
fn read_usage_values(
    sys: &mut System,
    samples_per_send: u8,
    sends_per_second: u8,
    AverageValues {
        cpu: prev_cpu_avg,
        mem: prev_mem_avg,
    }: AverageValues,
) -> AverageValues {
    let mut cpu_avg: u8 = 0;
    let mut mem_avg: u8 = 0;

    for _ in 0..samples_per_send {
        // refresh values before reading
        sys.refresh_specifics(
            RefreshKind::new()
                .with_memory(MemoryRefreshKind::new().with_ram())
                .with_cpu(CpuRefreshKind::new().with_cpu_usage()),
        );

        // average the cpu usage over all cores
        let cpu_usage =
            sys.cpus().iter().map(|cpu| cpu.cpu_usage()).sum::<f32>() / sys.cpus().len() as f32;
        cpu_avg += cpu_usage as u8;

        // read memory usage
        let mem_usage: f32 = sys.used_memory() as f32 * 100.0 / sys.total_memory() as f32;
        mem_avg += mem_usage as u8;

        // wait appropiate time before reading values again
        // according to configuration of samples and sends
        let dur =
            Duration::from_secs_f32(1.0 / (samples_per_send as f32 * sends_per_second as f32));
        tracing::debug!(
            "Read CPU: {:5.1} and MEM: {:5.1}. Waiting {} milliseconds before re-reading values.",
            cpu_usage,
            mem_usage,
            dur.as_millis()
        );
        thread::sleep(dur);
    }

    cpu_avg = (0.7 * (cpu_avg / samples_per_send) as f32 + 0.3 * prev_cpu_avg as f32) as u8;
    mem_avg = (0.7 * (mem_avg / samples_per_send) as f32 + 0.3 * prev_mem_avg as f32) as u8;

    if cpu_avg > 100 {
        cpu_avg = 100;
    }
    if mem_avg > 100 {
        mem_avg = 100;
    }

    AverageValues {
        cpu: cpu_avg,
        mem: mem_avg,
    }
}

#[tracing::instrument]
fn connect_serial_port(port_name: String, baud_rate: u32) -> anyhow::Result<Box<dyn SerialPort>> {
    Ok(serialport::new(port_name, baud_rate)
        .timeout(Duration::from_millis(100))
        .open()?)
}

#[tracing::instrument]
fn write_configuration(
    port: &mut Box<dyn SerialPort>,
    meter_calibration: MeterCalibration,
    led_brightness: LEDBrightness,
    threshold_percent: u8,
) -> anyhow::Result<()> {
    let bytes: [u8; 10] = [
        b'C',
        meter_calibration.cpu_min,
        meter_calibration.cpu_max,
        meter_calibration.mem_min,
        meter_calibration.mem_max,
        led_brightness.cpu_green,
        led_brightness.cpu_red,
        led_brightness.mem_green,
        led_brightness.mem_red,
        threshold_percent,
    ];
    tracing::info!("Sending configuration: '{bytes:?}'");
    port.write_all(&bytes)?;
    Ok(())
}

#[tracing::instrument]
fn write_values(port: &mut Box<dyn SerialPort>, cpu: u8, mem: u8) -> anyhow::Result<()> {
    let bytes: [u8; 3] = [b'V', cpu, mem];
    tracing::info!("Sending values: '{bytes:?}'");
    port.write_all(&bytes)?;
    Ok(())
}

#[tracing::instrument]
fn main() -> anyhow::Result<()> {
    // construct a subscriber that prints formatted traces to stdout
    let subscriber = tracing_subscriber::FmtSubscriber::new();
    // use that subscriber to process traces emitted after this point
    tracing::subscriber::set_global_default(subscriber)
        .map_err(|e| anyhow!("Failed to initialize logging: {e}."))?;

    let config = load_config().map_err(|e| anyhow!("Failed to load config file: {e}."))?;
    tracing::info!("Loaded configuration: {:?}", config);

    let mut port = connect_serial_port(config.port, config.baud)
        .map_err(|e| anyhow!("Failed to open port: {e}."))?;

    // send the initial configuration for the meter
    write_configuration(
        &mut port,
        config.meter_calibration_8bit,
        config.led_brightness_8bit,
        config.threshold_percent,
    )
    .map_err(|e| anyhow!("Failed to write to port: {e}."))?;

    // loop: read values and write to serial port
    let mut sys = System::new_all();
    let mut values = AverageValues { cpu: 0u8, mem: 0u8 };
    loop {
        values = read_usage_values(
            &mut sys,
            config.samples_per_send,
            config.sends_per_second,
            values,
        );
        write_values(&mut port, values.cpu, values.mem)
            .map_err(|e| anyhow!("Failed to write to port: {e}."))?;
    }
}
